"""

This is the code of the entire program SPG

Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

# -*- coding: utf-8 -*-

from random import randrange
from sys import argv


class spg:

    def __init__(self):
        # 30 special characters
        self.special = ['0', ',', ';', '1', ':', '!', '2', 'ù', '*', '3', '$',
                        '&', '4', 'é', '"', '5', '\'', '(', '6', '-', 'è',
                        '7', '_', 'ç', '8', 'à', ')', '9', '=', '<']

    def genPasswd(self, nbChar, specialChars):
        passwdList = []
        if specialChars:
            for i in range(nbChar):
                randNb = randrange(0, 82)
                if randNb <= 25:
                    randNb += 65
                    randNb = chr(randNb)
                elif randNb <= 51:
                    randNb -= 25
                    randNb += 96
                    randNb = chr(randNb)
                else:
                    randNb -= 52
                    randNb = self.special[randNb]
                passwdList.append(randNb)
        else:
            for i in range(nbChar):
                randNb = randrange(0, 52)
                if randNb <= 25:
                    randNb += 65
                    randNb = chr(randNb)
                else:
                    randNb -= 25
                    randNb += 96
                    randNb = chr(randNb)
                passwdList.append(randNb)
        passwd = ''.join(passwdList)
        return passwd


def checkArgv():
    if len(argv) < 2:
        print("Usage: spg.py [-s] <length>")
        print("The -s enable numbers and special characters")
        return False
    else:
        return True

if checkArgv():
    test = spg()
    if argv[1] == '-s':
        passwd = test.genPasswd(int(argv[2]), 1)
    else:
        passwd = test.genPasswd(int(argv[1]), 0)
    print(passwd)
